# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

PYTHON_COMPAT=( python{2_6,2_7} )

inherit cmake-utils eutils python-single-r1

DESCRIPTION="C++/Python interface of FEniCS"
HOMEPAGE="https://bitbucket.org/fenics-project/dolfin/"
SRC_URI="https://bitbucket.org/fenics-project/${PN}/downloads/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86 amd64"
IUSE="vtk cgal +viper"

DEPEND="
	dev-libs/boost
	dev-libs/libxml2:2
	sci-libs/armadillo
	>=sci-mathematics/ufc-2.3.0
	${PYTHON_DEPS}
	>=dev-python/ufl-1.3.0
	>=dev-python/ffc-1.3.0
	dev-python/fiat
	>=dev-python/instant-1.3.0
	dev-python/ply
	dev-util/cppunit
	dev-cpp/eigen:3
	dev-libs/boost[mpi]
	vtk? ( <=sci-libs/vtk-5.10.1 )
	sci-libs/umfpack
	"
RDEPEND="${DEPEND}"

pkg_setup() {
	python-single-r1_pkg_setup
}
