# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

PYTHON_COMPAT=( python{2_6,2_7,3_2,3_3} )

inherit distutils-r1

DESCRIPTION="Instant inlining of C and C++ code in Python"
HOMEPAGE="https://bitbucket.org/fenics-project/instant/"
SRC_URI="https://bitbucket.org/fenics-project/instant/downloads/instant-1.3.0.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 amd64"
IUSE=""
