# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EPAI=4
inherit flag-o-matic 

DESCRIPTION="A C++ templated sparse matrix library"
HOMEPAGE="http://cusplibrary.github.io/"
SRC_URI="https://cusp-library.googlecode.com/files/cusp-v0.3.1.zip"
KEYWORDS="amd64 x86"

LICENSE="Apache 2.0"
SLOT="0"

DEPEND="dev-util/nvidia-cuda-toolkit
	app-arch/unzip"
RDEPEND="${DEPEND}"

src_install() {
    dodir /opt/cuda/include/
    cp -r "${WORKDIR}/cusp" "${D}opt/cuda/include/"
}
