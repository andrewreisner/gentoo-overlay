# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EPAI=4
inherit flag-o-matic git-2

DESCRIPTION="A C++ templated sparse matrix library"
HOMEPAGE="http://cusplibrary.github.io/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/cusplibrary/cusplibrary.git"
KEYWORDS=""

LICENSE="Apache 2.0"
SLOT="0"

DEPEND="dev-util/nvidia-cuda-toolkit"
RDEPEND="${DEPEND}"

src_unpack() {
    git-2_src_unpack
}

src_install() {
    cd "${S}"
    dodir /opt/cuda/include/
    cp -r cusp "${D}opt/cuda/include/"
}
