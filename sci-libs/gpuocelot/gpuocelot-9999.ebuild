# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"
inherit subversion autotools flag-o-matic

DESCRIPTION="A dynamic compilation framework for PTX"
HOMEPAGE="http://code.google.com/p/gpuocelot"
SRC_URI=""
ESVN_REPO_URI="http://gpuocelot.googlecode.com/svn/trunk/ocelot/"
KEYWORDS=""

LICENSE="BSD"
SLOT="0"

DEPEND=">=dev-libs/boost-1.45.0
	dev-util/nvidia-cuda-toolkit
	media-libs/glew
	dev-util/scons
	>=sys-devel/llvm-3.3"
RDEPEND="${DEPEND}"

src_unpack() {
	     subversion_src_unpack
}

src_install() {
	      cd "${S}"
	      ./build.py -p "${D}/usr" --install
}
